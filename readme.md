# Roomonitor Meteor programmers test

This repo contains an exercise based in Meteor 1.6.0.1

## The Challenge

* Follow the [TODO webapp tutorial for Meteor + React](https://react-tutorial.meteor.com/) and complete it in this code

* Add login and registration screen and protect the TODO app by login.

* Each user has their own todo list elements

* Add an API endpoints for getting the TODO list of one user using [Restivus](https://atmospherejs.com/nimble/restivus)

* Use [Material UI](https://medium.com/@derrybirkett/how-to-code-a-quick-job-board-with-react-meteor-and-material-ui-d4ab3b619ec3#c872) for designing the registration and login screens 

## The result code of the exercise

Please send us a git repository from your side with the code of the exercise.

## Tooling

You need to install [meteor cli tool](https://www.meteor.com/install) in your computer for running the code.